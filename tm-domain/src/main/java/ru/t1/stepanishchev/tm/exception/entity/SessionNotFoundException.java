package ru.t1.stepanishchev.tm.exception.entity;

public class SessionNotFoundException extends AbstractEntityNotFoundException {

    public SessionNotFoundException() {
        super("Error! Session not found...");
    }

}