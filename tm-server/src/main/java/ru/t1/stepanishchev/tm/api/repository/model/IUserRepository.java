package ru.t1.stepanishchev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.model.User;

import java.util.List;

public interface IUserRepository extends IAbstractRepository<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    User findOneById(@Nullable String id);

    @Nullable
    User findOneByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    void clear();

}