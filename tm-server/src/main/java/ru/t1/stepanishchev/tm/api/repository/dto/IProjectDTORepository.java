package ru.t1.stepanishchev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAll(@NotNull String userId, @NotNull String sort);

    @Nullable
    ProjectDTO findOneById(@NotNull String userId, @NotNull String id);

    void clear();

    void clear(@NotNull String userId);

}