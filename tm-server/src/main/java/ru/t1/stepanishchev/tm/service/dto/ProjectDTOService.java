package ru.t1.stepanishchev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.stepanishchev.tm.api.service.IConnectionService;
import ru.t1.stepanishchev.tm.api.service.dto.IProjectDTOService;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.enumerated.Status;
import ru.t1.stepanishchev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.stepanishchev.tm.exception.entity.StatusEmptyException;
import ru.t1.stepanishchev.tm.exception.field.IdEmptyException;
import ru.t1.stepanishchev.tm.exception.field.NameEmptyException;
import ru.t1.stepanishchev.tm.exception.field.UserIdEmptyException;
import ru.t1.stepanishchev.tm.repository.dto.ProjectDTORepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;
import java.util.List;

public final class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO, IProjectDTORepository>
        implements IProjectDTOService {

    public ProjectDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public IProjectDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new ProjectDTORepository(entityManager);
    }

    @Override
    @NotNull
    public String getSortType(@Nullable final ProjectSort sort) {
        if (sort == ProjectSort.BY_CREATED) return "created";
        if (sort == ProjectSort.BY_STATUS) return "status";
        else return "name";
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear(userId);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.clear();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        try {
            return repository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        try {
            return repository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        try {
            return repository.findAll(userId, getSortType(sort));
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        try {
            @Nullable final ProjectDTO project = repository.findOneById(userId, id);
            return project;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    @SneakyThrows
    public ProjectDTO removeOneById(
            @Nullable String userId,
            @Nullable final String id
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public void set(@NotNull final Collection<ProjectDTO> projects) {
        if (projects.isEmpty()) return;
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.addAll(projects);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateOneById(
            @Nullable String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IProjectDTORepository repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(project);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

}