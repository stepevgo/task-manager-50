package ru.t1.stepanishchev.tm.api.repository.model;

import ru.t1.stepanishchev.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IAbstractRepository<M> {

}