package ru.t1.stepanishchev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.model.AbstractModel;

import java.util.Collection;

public interface IAbstractRepository<M extends AbstractModel> {

    void add(@NotNull M model);

    void addAll(@NotNull Collection<M> models);

    void update(@NotNull M model);

    void remove(@NotNull M model);

}