package ru.t1.stepanishchev.tm.api.service.model;

import ru.t1.stepanishchev.tm.api.repository.model.IAbstractRepository;
import ru.t1.stepanishchev.tm.model.AbstractModel;

public interface IAbstractService<M extends AbstractModel> extends IAbstractRepository<M> {
}